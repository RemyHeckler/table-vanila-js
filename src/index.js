import data from '../assignment.json';
import '../styles/main.scss';

function currenciesCell(key, cell) {
  const perEach = document.createElement('div');

  perEach.innerHTML = key;
  cell.appendChild(perEach);
}

const { brands } = data;
brands.forEach((item) => {
  const title = document.createElement('div');
  const { campaigns } = item;

  title.innerHTML = item.name;
  document.getElementById('table').appendChild(title);
  campaigns.forEach((cmp) => {
    const tableForOneCampaign = document.createElement('div');
    const campaignName = document.createElement('div');
    const headCampaignName = document.createElement('div');
    const bodyCampaignName = document.createElement('div');
    const infoForOneCampaign = document.createElement('div');

    headCampaignName.innerHTML = 'Campaign';
    headCampaignName.className = 'border';
    bodyCampaignName.className = 'long-column left-column border';
    campaignName.className = 'flex column';
    campaignName.appendChild(headCampaignName);
    bodyCampaignName.innerHTML = cmp.name;
    campaignName.appendChild(bodyCampaignName);
    const tableHeader = ['Date', 'Clicks', 'Registrations', 'Currency', 'First deposits count', 'Deposits sum', 'NGR', 'Partner\'s income'];
    const headTable = document.createElement('div');

    tableHeader.forEach((cell) => {
      const name = document.createElement('div');
      name.className = 'cell-width center border';
      name.innerHTML = cell;
      headTable.appendChild(name);
    });
    infoForOneCampaign.appendChild(headTable);
    headTable.className = 'flex justify-start';
    infoForOneCampaign.className = 'full-width';
    tableForOneCampaign.className = 'flex justify-start';
    tableForOneCampaign.appendChild(campaignName);
    tableForOneCampaign.appendChild(infoForOneCampaign);
    const info = cmp.data;

    info.forEach((inf) => {
      const infoRow = document.createElement('div');
      const dateCell = document.createElement('div');
      const clicksCell = document.createElement('div');
      const registrationsCell = document.createElement('div');
      const codeCell = document.createElement('div');
      const firstDepositCell = document.createElement('div');
      const sumDepositCell = document.createElement('div');
      const ngrCell = document.createElement('div');
      const incomeCell = document.createElement('div');

      infoRow.className = 'flex justify-between';
      const {
        date, clicks, registrations, currencies,
      } = inf;

      dateCell.className = 'cell-width center border';
      clicksCell.className = 'cell-width center border';
      registrationsCell.className = 'cell-width center border';
      codeCell.className = 'cell-width center border';
      firstDepositCell.className = 'cell-width center border';
      sumDepositCell.className = 'cell-width center border';
      ngrCell.className = 'cell-width center border';
      incomeCell.className = 'cell-width center border';
      dateCell.innerHTML = date;
      infoRow.appendChild(dateCell);
      clicksCell.innerHTML = clicks;
      infoRow.appendChild(clicksCell);
      registrationsCell.innerHTML = registrations;
      infoRow.appendChild(registrationsCell);
      currencies.forEach((curr) => {
        const {
          code, deposits_sum, first_deposits_count, ngr, partner_income,
        } = curr;

        currenciesCell(code, codeCell);
        currenciesCell(first_deposits_count, firstDepositCell);
        currenciesCell(deposits_sum, sumDepositCell);
        currenciesCell(ngr, ngrCell);
        currenciesCell(partner_income, incomeCell);
      });
      infoRow.appendChild(codeCell);
      infoRow.appendChild(firstDepositCell);
      infoRow.appendChild(sumDepositCell);
      infoRow.appendChild(ngrCell);
      infoRow.appendChild(incomeCell);
      infoForOneCampaign.appendChild(infoRow);
      document.getElementById('table').appendChild(tableForOneCampaign);
    });
  });
});
